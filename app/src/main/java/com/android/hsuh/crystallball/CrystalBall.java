package com.android.hsuh.crystallball;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Handler;

public class CrystalBall extends AppCompatActivity {

    public TextView answerText;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float acceleration;
    private float currentAcceleration;
    private float previousAcceleration;



    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values [0];
            float y = event.values [1];
            float z = event.values [2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = (float)Math.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration * 0.9f + delta;

            if (acceleration >15) {
                Toast toast = Toast.makeText(getApplication(), "Device has shaken", Toast.LENGTH_SHORT);
                toast.show();
                MediaPlayer mediaPlayer = MediaPlayer.create(CrystalBall.this, R.raw.crystal_ball);
                mediaPlayer.start();
                answerText = (TextView) findViewById(R.id.answerText);
                answerText.startAnimation(slideintop);
                answerText.setText(Predictions.get().getPrediction());

                Handler textAnimation = new Handler();

                textAnimation.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        answerText.setText(Predictions.get().getPrediction());
                        animationslideintop = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.abc_slide_in_top);
                        answerText.startAnimation(animationslideintop);

                        List<String> list = new ArrayList<String>();
                        list.add("Your wish will come true");
                        list.add("Your wish will never come true");
                        list.add("Go for it");
                        list.add("Don't try it");
                        list.add("Seems like your lucky day");
                        list.add("Try again later");
                        Random rand = new Random();
                        String random = list.get(rand.nextInt(list.size()));

                    }



                }, 200);


            }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crystal_ball);




        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        previousAcceleration = SensorManager.GRAVITY_EARTH;




        answerText = (TextView) findViewById(R.id.answerText);
        answerText.setText(Predictions.get().getPrediction());
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, accelerometer, sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);
    }
}

