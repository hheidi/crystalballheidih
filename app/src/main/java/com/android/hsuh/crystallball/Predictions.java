package com.android.hsuh.crystallball;

import android.view.animation.Animation;
import android.widget.TextView;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;
    public TextView answerText;
    public Animation slideintop;

    private Predictions () {
        int randomNumber = (int) ((Math.random() * 2) + 1);
        if(randomNumber == 1) {
            answers= new String[] {
              "fninedm"
            };
        }
        else if(randomNumber == 2) {
            answers = new String[] {
                    "Ypur dreams "
            };
        }
    }

    public  static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        return answers[1];

    }

}
